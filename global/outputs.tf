output "eks-master-iam-role-arn" {
  value = "${module.eks-master.eks-master-iam-role-arn}"
}
output "eks-node-instance-profile-name" {
  value = "${module.eks-worker-node.eks-node-instance-profile-name}"
}
output "eks-node-iam-role-arn" {
  value = "${module.eks-worker-node.eks-node-iam-role-arn}"
}

