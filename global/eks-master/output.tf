output "eks-master-iam-role-arn" {
  value = "${aws_iam_role.eks-role.arn}"
}