resource "aws_eks_cluster" "eks-cluster" {
  name            = "${var.cluster-name}"
  role_arn        = "${data.terraform_remote_state.eks-iam-role-arn.eks-master-iam-role-arn}"

  vpc_config {
    security_group_ids = ["${var.eks-sg}"]
    subnet_ids         = ["${var.eks-subnets}"]
  }
}
