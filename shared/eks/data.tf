data "terraform_remote_state" "eks-iam-role-arn"{
    backend = "s3"
    config = {
        bucket  = "cloud-kube-tfstates"
        key     = "global/iam/terraform.tfstate"
        region  = "us-east-1"
        encrypt = true
    }
}
