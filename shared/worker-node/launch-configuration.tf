locals {
  eks-worker-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${var.eks-endpoint}' --b64-cluster-ca '${var.eks-certificate-authority}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "eks-worker-node-launch-config" {
  associate_public_ip_address = true
  iam_instance_profile        = "${data.terraform_remote_state.eks-node-iam.eks-node-instance-profile-name}"
  image_id                    = "${data.aws_ami.eks-worker.id}"
  key_name                    = "${var.instance-key}"
  instance_type               = "t3.medium"
  name_prefix                 = "${format("%s-terraform-eks-workshop", var.cluster-name)}"
  security_groups             = ["${var.eks-worker-sg-id}"]
  user_data_base64            = "${base64encode(local.eks-worker-node-userdata)}"

  lifecycle {
    create_before_destroy = true
  }
}