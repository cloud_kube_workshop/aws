variable "vpc-cidr-block" {
  type = "string"
}
variable "cluster-name" {
  type = "string"
}
variable "availability-zone" {
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
variable "eks-subnet-names" {
  default = [
    "eks-subnet-1",
    "eks-subnet-2",
    "eks-subnet-3"]
}
variable "eks-cidr-blocks" {
  type = "list"
}