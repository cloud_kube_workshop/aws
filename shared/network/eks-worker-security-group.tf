resource "aws_security_group" "eks-worker-node-sg" {
  name        = "${format("%s-terraform-eks-worker-node", var.cluster-name)}"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${aws_vpc.eks-vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${
    map(
     "Name", "terraform-eks-worker-node",
     "kubernetes.io/cluster/${var.cluster-name}", "owned",
    )
  }"
}