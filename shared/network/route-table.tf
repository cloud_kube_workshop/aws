resource "aws_route_table" "eks-route-table" {
  vpc_id = "${aws_vpc.eks-vpc.id}"

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.eks-ig.id}"
  }
}

resource "aws_route_table_association" "eks-route-table-association" {
    count = "${length(var.eks-subnet-names)}"

    subnet_id = "${aws_subnet.eks-subnet.*.id[count.index]}"
    route_table_id = "${aws_route_table.eks-route-table.id}"
}

