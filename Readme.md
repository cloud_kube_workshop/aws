# Thanks and welcome to the EKS Workshop!
*Thanks for being part of this initiative, in this document you will find everything you need for the workshop*

#### In this document you will find:
* #### Pre-requisites (*You need to do this before the workshop!*)
* #### Step-by-step


## Pre-requisites
### - If you have Windows
1. Install chocolatey, please follow instructions here: https://chocolatey.org/install
> chocolatey is the package manager for windows, if you have used Linux before is like yum or apt

2. Run the pre-requisites-windows.bat (**as admin**)

At the end of the execution you must see a message similar to the following:

```bash
Terraform v0.11.10

Your version of Terraform is out of date! The latest version
is 0.12.3. You can update by downloading from www.terraform.io/downloads.html
0
aws-cli/1.16.193 Python/3.6.0 Windows/10 botocore/1.12.183
0
Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.1", GitCommit:"4ed3216f3ec431b140b1d899130a69fc671678f4", GitTreeState:"clean", BuildDate:"2018-10-05T16:46:06Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"windows/amd64"}
Unable to connect to the server: dial tcp [::1]:8080: connectex: No connection could be made because the target machine actively refused it.
1
```

> If the script fails please go to help/pre-requisites.md file and follow the instructions, anyway please ping us (Zamir Peña | Andres Scarpetta | Maria Camila Peña on slack)

### - If you have MacOS/Linux

1. Make sure you have python and pip installed
[Here is a tutorial for Linux](https://docs.python-guide.org/starting/install3/linux/) and [MacOS](https://wsvincent.com/install-python3-mac/) for installing Python and pip (Version 3)

To check if you have `Python` and `pip`, on the console type:

```bash
python3 --version
pip3 --version
```
or
```bash
python --version
pip --version
```

The outcome must be something like this:

```bash
Python 3.7.2

pip 19.0.2 from path/to/pip (python 3.7)
```

2. Execute the following commands:

```bash
export PATH="$PATH:/opt/eks_workshop/terraform/"
export PATH="$PATH:/opt/eks_workshop/kubectl/"
```

3. Execute the ```pre-requisites-mac-linux.sh``` script (**as root**)

At the end of the execution you must see a message similar to the following:

```bash
Checking versions of your packages
Terraform
Terraform v0.11.11

Your version of Terraform is out of date! The latest version
is 0.12.3. You can update by downloading from www.terraform.io/downloads.html

aws-cli
aws-cli/1.16.104 Python/3.6.8 Linux/3.10.0-862.11.6.el7.x86_64 botocore/1.12.94

Kubectl
kubectl controls the Kubernetes cluster manager.

Find more information at: https://kubernetes.io/docs/reference/kubectl/overview/

Basic Commands (Beginner):
  create         Create a resource from a file or from stdin.
  expose         Take a replication controller, service, deployment or pod and expose it as a new Kubernetes Service
  run            Run a particular image on the cluster
  set            Set specific features on objects

Basic Commands (Intermediate):
  explain        Documentation of resources
  get            Display one or many resources
  edit           Edit a resource on the server
  delete         Delete resources by filenames, stdin, resources and names, or by resources and label selector

Deploy Commands:
  rollout        Manage the rollout of a resource
  scale          Set a new size for a Deployment, ReplicaSet, Replication Controller, or Job
  autoscale      Auto-scale a Deployment, ReplicaSet, or ReplicationController

Cluster Management Commands:
  certificate    Modify certificate resources.
  cluster-info   Display cluster info
  top            Display Resource (CPU/Memory/Storage) usage.
  cordon         Mark node as unschedulable
  uncordon       Mark node as schedulable
  drain          Drain node in preparation for maintenance
  taint          Update the taints on one or more nodes

Troubleshooting and Debugging Commands:
  describe       Show details of a specific resource or group of resources
  logs           Print the logs for a container in a pod
  attach         Attach to a running container
  exec           Execute a command in a container
  port-forward   Forward one or more local ports to a pod
  proxy          Run a proxy to the Kubernetes API server
  cp             Copy files and directories to and from containers.
  auth           Inspect authorization

Advanced Commands:
  apply          Apply a configuration to a resource by filename or stdin
  patch          Update field(s) of a resource using strategic merge patch
  replace        Replace a resource by filename or stdin
  wait           Experimental: Wait for a specific condition on one or many resources.
  convert        Convert config files between different API versions

Settings Commands:
  label          Update the labels on a resource
  annotate       Update the annotations on a resource
  completion     Output shell completion code for the specified shell (bash or zsh)

Other Commands:
  alpha          Commands for features in alpha
  api-resources  Print the supported API resources on the server
  api-versions   Print the supported API versions on the server, in the form of "group/version"
  config         Modify kubeconfig files
  plugin         Provides utilities for interacting with plugins.
  version        Print the client and server version information

Usage:
  kubectl [flags] [options]

Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).


```

4. **Be sure the output DOES NOT contain something like**

```bash
ERROR!!!!!!!!!!!!!
```

otherwise follow the instructions at help/pre-requisites.md file, anyway please ping us (Zamir Peña | Andres Scarpetta | Maria Camila Peña on slack)

### - For Windows/Mac/Linux
1. Check everything is installed, so, run:

```bash
terraform -version
```

```bash
aws --version
```

```bash
kubectl version
```

Every command should return the version of the software installed.

-----------------------------

# ¡Congratulations!
> ### You have everything for the workshop, see you there!

-----------------------------

# Step by Step
## Make yourself as root (if Linux/MacOs)
```bash
sudo -s
```

## Export environment variables
Execute the following commands:
### If MacOs/Linux
```bash
export AWS_ACCESS_KEY_ID=<paste the accesss key id>
export AWS_SECRET_ACCESS_KEY=<paste the secret accesss key id>
export AWS_DEFAULT_REGION=us-east-1
```

### If Windows
```bash
set AWS_ACCESS_KEY_ID=<paste the accesss key id>
set AWS_SECRET_ACCESS_KEY=<paste the secret accesss key id>
set AWS_DEFAULT_REGION=us-east-1
```

## Let's deploy the EKS Cluster

In order to achieve this deploy, please move into the repository folder that you clone earlier and follow the next instructions depending on your operating system:

1. Change to "**shared**" directory

* For Mac, Linux and Windows users who prefer to work on git bash and wsl, please run the script named `createEKS.sh`.
* For Windows users that use Powershell, please run the script named `createEKS.ps1`.

When you run it, no matter where, you will be asked for the name of the cluster which must have the next pattern:

```
<insert your username of endava here, for example if you are Andres Lopez, type "alopez">
```

This process will take around 12 minutes, so please be patient, check the terminal while the script is running

## Check the deployment

To check that everything works fine and the kubernetes configuration went well, let's follow the next:

* From the AWS web console, go to the service tab and search for EKS; in there you will see your cluster up and running. To look if the EC2 worker virtual machines went up well, go to the service tab and search for EK2, and you will see 3 machines running with the name "<insert a name here>".
* To look if your worker nodes are connected to the EKS cluster, please type or copy the following command, and you will look a similar output as is described bellow the command:

```bash
kubectl get nodes
```

```
NAME                         STATUS   ROLES    AGE     VERSION
ip-10-0-1-63.ec2.internal    Ready    <none>   2m37s   v1.12.7
```

After following this, you will be able to start to deploy application on kubernetes

## Create a Secret
This is important for the backend and database PODs due to the password of the database is injected to the containers throught this object (Secret)

Perform the following command in order to create the secret called **db-services**

```bash
kubectl create secret generic db-services --from-literal=DB_PASS=V2VsY29tZTIwMTgh
```

Then review that the secret is successfully created with:

```bash
kubectl get secrets
```

The output should be something like:

```bash
NAME                  TYPE                                  DATA   AGE
db-services           Opaque                                1      6m17s
default-token-4xtxf   kubernetes.io/service-account-token   3      18m
```

## Deploy the application components
Go to the "**k8s**" folder inside the repository and perform the following commands

#### Create the database k8s components
```bash
kubectl apply -f database.yaml
```

#### Create the backend k8s components
```bash
kubectl apply -f backend.yaml
```

#### Create the frontend k8s components
```bash
kubectl apply -f frontend.yaml
```

## Track the components creation
#### For pods
Perform the following command for knowing the state of your pods

```bash
kubectl get pods
```

The output should be something like:

```bash
NAME                         READY   STATUS    RESTARTS   AGE
movie-api-5f65446459-kmvr2   1/1     Running   0          20m
movie-db-986bb6b79-dm5xw     1/1     Running   0          20m
movie-ui-8599568d96-hv2j6    1/1     Running   0          20m
```

Perform the following command for knowing the state of your deployments
#### For deployments
```bash
kubectl get deployments
```

The output should be something like:

```bash
NAME        DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
movie-api   1         1         1            1           22m
movie-db    1         1         1            1           22m
movie-ui    1         1         1            1           22m
```

Perform the following command for knowing the state of your services
#### For services
```bash
kubectl get services
```

The output should be something like:

```bash
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)          AGE
kubernetes   ClusterIP      172.20.0.1       <none>                                                                    443/TCP          30m
movie-api    ClusterIP      172.20.210.194   <none>                                                                    3000/TCP         22m
movie-db     ClusterIP      172.20.230.237   <none>                                                                    3306/TCP         22m
movie-ui     LoadBalancer   172.20.125.97    aab1306f5a41711e998f90205dc5898d-1579039899.us-east-1.elb.amazonaws.com   3030:31622/TCP   22m
```

## Describe your components
Use `Kubectl describe <services, pods, deployments, secrets>: More information regarding services, pods, deployments, secrets, and so on.`

Services
```bash
kubectl describe services
```

Deployments
```bash
kubectl describe deployments
```

Pods
```bash
kubectl describe pods
```

## Access your application
In order to access to your application follow the next steps:
1. Describe the **movie-ui** service
2. Look for the **EXTERNAL-IP** field and take the dns
3. Look for the **PORT(S)** field and take the first port
4. Open a browser and access with the **URL** *(dns:port)*